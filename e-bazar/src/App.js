import React from 'react';
import ReactDOM from 'react-dom';

// const Homescreen= React.lazy(()=> import('./Components/Homescreen')); 
// const Topbar= React.lazy(()=> import('./Components/Topbar')); 
// const SignIn= React.lazy(()=> import('./Components/SignIn')); 
// const Sidebar= React.lazy(()=> import('./Components/Sidebar')); 

import Sidebar from '../src/Sidebar';
import Topbar from '../src/Topbar';
import Homescreen from '../src/Components/Homescreen';

// class App extends Component {
//   render() {
//     return (
//       <Suspense fallback={<div className="centered"></div>}>
//         <Router>
//           <Homescreen/>
//         </Router>
//       </Suspense>
//     );
//   }
// }

class App extends React.Component {
  render() {
    return (
      <div>
     <Sidebar />
    <Topbar />
 <Homescreen/>
      </div>
    );
  }
}

export default App;
